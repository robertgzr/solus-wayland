#!/bin/bash

set -eo pipefail 
shopt -s nullglob

pkgdir=${PKGDIR:-./pkg}

for p in $pkgdir/**/*.eopkg; do
    dirname=`dirname $p`
    [[ $dirname =~ .*(common)$ ]] && continue

    mkdir -p ./repo/`basename $dirname`/
    mv -v $p ./repo/`basename $dirname`/
done

all: repo static

repo:
	exec ./contrib/extract-eopkg.sh
	cd repo && eopkg index .

container:
	docker build -t robertgzr/solus-wayland -f contrib/Dockerfile .

update-system:
	@sudo ./update.sh

# run by gitlab ci:
static:
	rm -rf ./public/*
	caddygen -root ./repo -public ./public

.PHONY: repo static container update-system

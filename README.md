# solus-wayland

https://robertgzr.gitlab.io/solus-wayland/


## How do I use this?

```sh
$ # add to repos
$ eopkg add-repo Wayland https://robertgzr.gitlab.io/solus-wayland/eopkg-index.xml.xz
Repo Wayland added to system.
Updating repository: Wayland
eopkg-index.xml.xz.sha1sum     (40.0  B)100%    476.28 KB/s [00:00:00] [complete]
eopkg-index.xml.xz             (1.0 KB)100%     16.31 MB/s [00:00:00] [complete]
Package database updated.

$ # make sure it worked
$ eopkg list-repo
Solus [active]
   https://mirrors.rit.edu/solus/packages/shannon/eopkg-index.xml.xz
Wayland [active]
   https://robertgzr.gitlab.io/solus-wayland/eopkg-index.xml.xz
```

## Development

1. Update the packages under `pkg/`
2. Build them using `solbuild` (you should use the provided Makefile)
3. Extract the eopkg files into `repo/` and rebuild the index with:

```sh
$ make repo
```

4. Push to gitlab and let the CI refresh the repo

#!/bin/sh

eopkg disable-repo Solus
trap '{ eopkg enable-repo Solus; }' EXIT
eopkg upgrade
